const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { mongoose } = require('./db.js');
var roomController = require('./controller/roomController.js');
var blockController = require('./controller/blockController.js');
var collegeController = require('./controller/collegeController.js');

var app = express();
app.use(bodyParser.json());
app.use(cors({origin:'http://localhost:4200'}));

app.listen(3000,() =>
    console.log('Server started at port : 3000')
);

app.use('/room', roomController);
app.use('/block', blockController);
app.use('/college', collegeController);